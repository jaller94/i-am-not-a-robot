// Just a file used for debugging image.js

import {
  generateCanvas,
  storeCanvas,
} from './image.js';

/**
 * Returns a random integer
 * @returns {number}
 */
function randomInt(min, max) {  
  return Math.floor(Math.random() * (max - min) + min);
}

function deg2rad(deg) {
  return deg % 360 * Math.PI / 180;
}

async function main() {
  for(let i = 0; i < 10; i++) {
    const options = {
      mainHue: randomInt(0, 360),
      mainRotation: 45 * randomInt(0, 7),
      heartX: Math.random() * 2 - 1,
      heartY: Math.random() * 2 - 1,
    };
    console.dir(options);
    const canvas = generateCanvas(options);
    await storeCanvas(canvas, i);
    console.log(`finished ${i}`);
  }
}

main().catch(console.error);
