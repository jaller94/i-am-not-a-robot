import fs from 'node:fs';
import fsPromises from 'node:fs/promises';
import path from 'node:path';

import glitch from 'glitch-canvas';
import { createCanvas } from 'canvas';

export function generateCanvas({
  heartX,
  heartY,
  mainHue,
  mainRotation,
}) {
  const canvas = createCanvas(1024, 1024);
  const ctx = canvas.getContext('2d');

  ctx.fillStyle = '#808080';
  ctx.fillRect(0, 0, canvas.width, canvas.height);

  ctx.translate(canvas.width / 2, canvas.height / 2);

  ctx.font = '256px sans-serif';
  ctx.fillStyle = 'red';
  ctx.fillText('♥', heartX * canvas.width / 2, heartY * canvas.width / 2);

  ctx.rotate(mainRotation);

  ctx.lineWidth = 64;
  for(let i = -1; i <= 1; i++) {
    const color = ((mainHue + 45 * i) + 720) % 360;
    ctx.strokeStyle = `hsl(${color}, 86%, 50%)`;
    ctx.beginPath();
    ctx.lineTo(-1000, 168 * i);
    ctx.lineTo(1000, 168 * i);
    ctx.stroke();
  }

  return canvas;
}

export function toGlitchedJPGStream(canvas) {
  return glitch({
    seed:       25, // integer between 0 and 99
    quality:    60, // integer between 0 and 99
    amount:     35, // integer between 0 and 99
    iterations: 10, // integer
  })
    .fromBuffer( canvas.toBuffer() )
    .toJPGStream({
      bufsize: 4096,          // output buffer size in bytes
      quality: 60,            // jpg quality (0-100) default: 75
      progressive: true       // true for progressive compression
    });
}

export function storeCanvas(canvas, i, folderPath = '') {
  const outputJPGStream = fs.createWriteStream(path.join(folderPath, `glitched-${i}.jpg`));
  //const outputPNGStream = fs.createWriteStream(`./imgs/glitched-${i}.png`);
  // glitch({
  //   seed:       25, // integer between 0 and 99
  //   quality:    60, // integer between 0 and 99
  //   amount:     35, // integer between 0 and 99
  //   iterations: 10, // integer
  // })
  //   .fromBuffer( canvas.toBuffer() )
  //   .toPNGStream()
  //   .then((pngStream) => {
  //     pngStream.on( 'data', function ( chunk ) { outputPNGStream.write( chunk ); } );
  //     pngStream.on( 'end', function () { console.log( 'png file saved.' ); } );
  //   });

  const promise1 = toGlitchedJPGStream(canvas)
    .then((jpgStream) => {
      jpgStream.on( 'data', function ( chunk ) { outputJPGStream.write( chunk ); } );
      jpgStream.on( 'end', function () { outputJPGStream.close() } );
    });

  const promise2 = fsPromises.writeFile(path.join(folderPath, `out-${i}.svg`), canvas.toBuffer());

  return Promise.all([promise1, promise2]);
}
