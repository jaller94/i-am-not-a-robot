import {
  generateCanvas,
} from './image.js';

describe('generateCanvas', () => {
  test('fails without parameters', () => {
    expect(() => {
      generateCanvas();
    }).toThrow();
  });
  test('succeeds with parameters 1', () => {
    const options = {
      mainHue: 252,
      mainRotation: 0,
      heartX: 0.702,
      heartY: -0.4
    };
    expect(generateCanvas(options)).toBeTruthy();
  });
  test('succeeds with parameters 2', () => {
    const options = {
      mainHue: 252,
      mainRotation: 0,
      heartX: 0.7029,
      heartY: -0.4
    };
    expect(generateCanvas(options)).toBeTruthy();
  });
});
