import 'dotenv/config';
import fs from 'node:fs';
import path from 'node:path';
import Mastodon from '@jaller94/mastodon-api';

import {
  generateCanvas,
  storeCanvas,
} from './image.js';

/**
 * Returns a random integer
 * @returns {number}
 */
function randomInt(min, max) {
  return Math.floor(Math.random() * (max - min) + min);
}

const M = new Mastodon({
  client_key: process.env.CLIENT_KEY,
  client_secret: process.env.CLIENT_SECRET,
  access_token: process.env.ACCESS_TOKEN,
  timeout_ms: 60 * 1000, // optional HTTP request timeout to apply to all requests.
  api_url: process.env.API_URL, // optional, defaults to https://mastodon.social/api/v1/
});

const generateText = () => {
  const beings = 'The sun,The moon,The earth,The sky,The man,The boy,The girl,The fox,The cat,The dog,The bat,The bird,The fly,The fish,The duck,The frog,Sugar,Linux,The script,The poem,The poet,The word,The speech,The time,My heart,His heart,Her heart,My thought,His thought,Her thought,My mind,His mind,Her mind'.split(',');
  const adjectives = 'here,gone,mine,hers,his,theirs,sweet,free,calm,still,mute,hurt,kind,mild,pure,sad,mad,good,save,great'.split(',');
  const line = beings[randomInt(0, beings.length)] + ' is ' + adjectives[randomInt(0, adjectives.length)];
  const text = `Roses are red,\nViolets are blue,\n${line},\nAnd so are you.`;
  return text;
};

async function post(imageStream) {
  const imageResponse = await M.post('media', { file: imageStream });
  if (imageResponse.data.error) {
    throw new Error(`Server error while uploading image: ${imageResponse.data.error}`);
  }
  const params = {
    language: 'eng',
    status: generateText(),
    media_ids: [imageResponse.data.id],
    sensitive: false,
    visibility: 'unlisted',
  };
  const response = await M.post('statuses', params);
  if (response.data.error) {
    throw new Error(`Server error while posting: ${response.data.error}`);
  }
}

async function main() {
  const options = {
    mainHue: randomInt(0, 360),
    mainRotation: 45 * randomInt(0, 7),
    heartX: Math.random() * 2 - 1,
    heartY: Math.random() * 2 - 1,
  };
  //console.dir(options);
  const canvas = generateCanvas(options);
  const folderPath = 'imgs';
  await storeCanvas(canvas, 0, folderPath);
  await post(fs.createReadStream(path.join(folderPath, 'glitched-0.jpg')));
}

main().catch((err) => {
  console.error(err);
  process.exit(1);
});
