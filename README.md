# Microblogging bot: I am not a robot

This is a social media bot for [Mastodon](https://joinmastodon.org/). It posts a glitchy image, claiming not to be a robot.

Test account:
* https://botsin.space/@not_a_robot

## Run the application
[Join any Mastodon server](https://joinmastodon.org/) and register a client and an access token. If you don’t know which Mastodon instance to pick, [botsin.space](https://botsin.space/) is a popular instance only for bots.

Make sure to have the latest version of Node and NPM installed.

```sh
npm install               # installs dependencies
cp .env-sample .env
$EDITOR .env              # fill in client and access information
npm run-script post       # Creates one post
```
